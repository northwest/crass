#!/usr/bin/env python
import datetime

def Months():
	returnable = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
	returnable += ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"]

	return returnable

def Years():
	returnable = range(1900, datetime.datetime.now().year + 2)
	return returnable

def Seasons():
	returnable = ["Spring", "Summer", "Fall", "Winter"]
	return returnable

def Endings():
	returnable = []
	symbols = "!.?~`#"
	nums = range(0,9)
	for symbol in symbols:
		for bit in range(0,2):
			bb = bit - 1
			if not bit:
				for num in nums:
					returnable.append(symbol + str(num))
			else:
				for num in nums:
					returnable.append(symbol + str(bb) + str(num))
	return returnable


def GeneratePasswords(casing = 0):
	returnable = []
	for year in Years():
		for month in Months():
			p = month + str(year)
			if casing == 1:
				p = p.upper()
			if casing == -1:
				p = p.lower()
			print(p)

	for year in Years():
		for season in Seasons():
			p = season + str(year)
			if casing == 1:
				p = p.upper()
			if casing == -1:
				p = p.lower()
			print(p)
			for end in Endings():
				q = p + end
				print(q)
	return returnable			






if __name__ == "__main__":
	try:
		input("This is gonna output them to stdout, so you might wanna `>` into a file. Press enter to continue...")
	except:
		pass	#sssh, don't say it
	GeneratePasswords(-1)
	GeneratePasswords()
	GeneratePasswords(1)
